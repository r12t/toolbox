// Bookmarklet to create new note from selected text in browser
function getSelectionText() {
var text = "";
if (window.getSelection) {
text = window.getSelection().toString();
} else if (document.selection && document.selection.type != "Control") {
text = document.selection.createRange().text;
}
return text;
}

javascript:void(window.open("nvalt://make?txt="+document.getSelection()))
