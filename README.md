# toolbox

A toolbox with some useful scripts. Based on [rubenerd's](https://rubenerd.com) [lunchbox](https://codeberg.org/rubenerd/lunchbox).

Script | Purpose
--- | ---
`DraftsPrefixTaskpaperTasks.js` | Drafts 3 Action Step Script to prefix taskpaper tasks with hyphens (https://agiletortoise.zendesk.com/hc/en-us/articles/202771590-Action-Step-Script)
`hidedotfiles.sh` | Hide dotfiles in Finder. See `showdotfiles.sh`
`image2jpg.sh` | Convert any image to `.jpg` (useful for `.heic` files)
`lock-usb-disk.sh` | Script to automatically lock and unlock the computer when my usb pendrive is removed (https://askubuntu.com/questions/28836/lock-and-unlock-from-usb-disk-pendrive)
`Log to Day One.applescript` | Omnifocus -> Day One Daily Completed Task Log
`mr-reader-wallabag.js` | Bookmarklet to add a page to your self-hosted wallabag (http://wallabag.org) from Mr. Reader (http://www.curioustimes.de/mrreader/)
`pindown.py` | Convert pinboard `.json` export bookmarks to individual markdown files.
`showdotfiles.sh` | Show all files in Mac OS X Finder (e.g. inclucing dotfiles)
`todotxt.json` | A template for [ofexport](https://github.com/psidnell/ofexport) to export to todo.txt
`uri-escaped-plain-text.js` | Bookmarklet to create new note from selected text in browser

## mr-reader-wallabag.js

To add it to your Mr. Reader configuration, go to the Settings menu, choose Services, then Bookmarklets and paste the code into the javascript field. Call it something like 'bag it!'. It will only work in the 'Web' view. After running the javascript you will get to see your wallabag page. Just close the window and continue reading in Mr. Reader.
