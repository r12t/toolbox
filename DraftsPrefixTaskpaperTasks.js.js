// Based on Macdrifter script
// http://www.macdrifter.com/2015/01/the-drafts-inbox-for-plain-text-tasks.html

//   Function to remove multiple line breaks
function removeBlanks(txt){
    txt = txt.replace(/(\r\n|\r|\n)+/g, '$1')
    return txt;
}
// Function to Prefix each line of a text block
function textPrefix(textStr, preFix){
var i= 0;                
var array1 = textStr.split("\n");
// Regex for one or more tabs
re = /^\t{1,}/;
// Split the text into lines stored in an array
for ( i = 0; i < array1.length; i++) {
    // Skip lines that are just blank
    if (array1[i] !== ""){
        taskString = array1[i];
        var tabPrefix = "";
        // Find the tabs
        var tabMatches = re.exec(taskString);
        if (tabMatches){
            tabPrefix = tabMatches[0];
            // Temporarily remove the tabs so we can prefix it
            taskString = taskString.replace(/^\t{1,}/, '')
        }
        // Check to see if it's a project
        var projMatches = removeBlanks(array1[i]).match(/:$/);

        if (projMatches == null){
            // If it's not a project then add the hyphen and tabs
            array1[i] = tabPrefix + preFix + taskString;
        } else{
            // Otherwise just put the tabs back
            array1[i] = tabPrefix + taskString;
        }
    }
}
// Put all the lines back together
val = array1.join("\n");
return val;
}
// This is how we change the content in Drafts
var sel = draft.content;
// Call the function with a prefix for TaskPaper tasks
draft.content = (textPrefix(removeBlanks(sel), "- "));
commit(draft);