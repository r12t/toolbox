
#!/bin/sh
# https://blog.fsck.com/2023/12/13/copy-as-jpeg/

for f in "$@"
do
    rp=`realpath "$f"`
    dir="$(dirname "${rp}")"
    base="$(basename "${rp}")"
    name=${base%.*}
    outfile="$dir/$name.jpg"

    /usr/bin/sips -s format jpeg "$rp" --out "$outfile"
done
